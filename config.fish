if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias ct='cowthink'
alias minecraft='java -jar ~/Downloads/TLauncher-2.86.jar'
alias dmen='dmenu_run -nb "$color0" -nf "$color15" -sb "$color1" -sf "$color15"'
alias inst='sudo xbps-install -S'
alias delt='sudo xbps-remove -R'
alias srch='sudo xbps-query -Rs'
alias ls='ls --color=auto'
alias ll='ls -la'
alias wp='ranger ~/Pictures/Wallpapers'
alias rn='ranger'
alias feh='feh --scale-down --auto-zoom'
alias bedit='vim ~/.bashrc'
alias vedit='vim ~/.config/nvim/init.vim'
alias i3edit='vim ~/.config/i3/config'
alias ttefc='cd ~/repos/ttef/ttefc/'
alias cs='cowsay'
alias v='vim'
alias cm='cd ..'
alias 4c='ranger ~/Pictures/4chan/'
alias cdp='cd ~/Pictures'
alias wedit='sudo vim /etc/wpa_supplicant/wpa_supplicant-wlp3s0.conf'
alias cdd='cd ~/Documents/'
alias dl='youtube-dl'
alias adl='youtube-dl -x --audio-format flac --audio-quality 0 --add-metadata'
alias am='alsamixer'
alias cdm='cd ~/Music/'
alias cdv='cd ~/Videos/'
alias mktex='cp ~/template $(pwd)/template.tex'
alias htop='htop -t'
alias ftp='ftp -i'
alias rftp='ftp 192.168.2.114'
alias rrftp='ftp $ipaddrraspi'
alias redit='vim ~/.config/ranger/rc.conf'
alias lacon='sudo pvpn -l'
alias ccon='sudo pvpn -cc'
alias discon='sudo pvpn -d'
alias p2p='sudo pvpn -p2p'
alias vstat='sudo pvpn --status'
alias recon='sudo pvpn --reconnect'
alias cl='clear'
alias cdr='cd ~/Repos/'
alias theme='wal --theme'
alias rmconf='rm ~/.config/zathura/zathurarc'
alias cdpu='cd /etc/portage/package.use/'
alias svim='sudo vim'
alias z='zathura'
alias xv='cd ~/Pictures/4chan/stuff/vids'
alias inst='sudo xbps-install -S'
alias delt='sudo xbps-remove -R'
alias srch='xbps-query -Rs'
alias tedit='vim ~/todo'
alias tp='cp ~/Documents/latemp.tex'
alias H='cd ~/repos/Haskell/'
alias tmux="env TERM=xterm-256color tmux"
alias nm="neomutt"
alias n='ncmpcpp'

fish_vi_key_bindings

set -U fish_greeting

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin /home/nisse/.ghcup/bin $PATH # ghcup-env

export DENO_INSTALL="/home/nisse/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"
export PATH="/home/nisse/.juliaup/bin:$PATH"
export PATH="/home/nisse/.local/bin:$PATH"

