set showmatch
set listchars+=space:·
"set t_Co=256

"autocmd BufRead,BufNewFile *.txt,*.text,*.md,*.markdown,README setlocal textwidth=80 wrap linebreak
"autocmd BufRead,BufNewFile * if &filetype == '' | setlocal textwidth=80 wrap linebreak | endif



"autocmd InsertEnter * :set norelativenumber
"autocmd InsertLeave * :set relativenumber
set nocompatible    " be iMproved, required
set rtp+=~/.vim
set rtp+=~/.vim/after/syntax
syntax on
set hidden
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes
set termguicolors
set encoding=utf-8
set formatoptions+=w
set cursorline
filetype off                  " required
set laststatus=2
set pumheight=12
set colorcolumn=80
set background=dark

set number
autocmd InsertEnter * :set norelativenumber
autocmd InsertLeave * :set relativenumber

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'xuhdev/vim-latex-live-preview'
Plugin 'airblade/vim-gitgutter'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'neovim/nvim-lspconfig'
Plugin 'ray-x/lsp_signature.nvim'

Plugin 'NLKNguyen/papercolor-theme'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'neovimhaskell/haskell-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'mbbill/undotree'
Plugin 'itchyny/lightline.vim'
Plugin 'preservim/tagbar'
Plugin 'nvim-treesitter/nvim-treesitter'
Plugin 'folke/lsp-colors.nvim'
Plugin 'folke/trouble.nvim'
Plugin 'iamcco/markdown-preview.nvim'


Plugin 'hrsh7th/cmp-nvim-lsp'
Plugin 'hrsh7th/cmp-buffer'
Plugin 'hrsh7th/cmp-path'
Plugin 'hrsh7th/nvim-cmp'


Plugin 'quangnguyen30192/cmp-nvim-ultisnips'
Plugin 'easymotion/vim-easymotion'
Plugin 'tpope/vim-surround'
Plugin 'monkoose/fzf-hoogle.vim'
Plugin 'puremourning/vimspector'
"Plugin 'Olical/conjure'
"Plugin 'tribela/vim-transparent'

Plugin 'sainnhe/everforest'
Plugin 'sainnhe/edge'
Plugin 'sainnhe/gruvbox-material'
Plugin 'jeffkreeftmeijer/vim-dim'
Plugin 'joshdick/onedark.vim'
Plugin 'savq/melange'
Plugin 'wadackel/vim-dogrun'
Plugin 'tomasiser/vim-code-dark'
Plugin 'shaunsingh/nord.nvim'
Plugin 'olimorris/onedarkpro.nvim'
Plugin 'nekonako/xresources-nvim'
Plugin 'dylanaraps/wal.vim'
Plugin 'Mofiqul/vscode.nvim'

Plugin 'psliwka/vim-smoothie'
Plugin 'preservim/vim-wheel'
Plugin 'calebsmith/vim-lambdify'

Plugin 'lervag/vimtex'
Plugin 'zbirenbaum/copilot.lua'
Plugin 'lunacookies/vim-plan9'

call vundle#end()            " required
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

set autoread
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
autocmd FileChangedShellPost * echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None
nnoremap <silent> <LocalLeader>O :call pluto#insert_cell_above()<CR>
nnoremap <silent> <LocalLeader>o :call pluto#insert_cell_below()<CR>

nnoremap <silent> <LocalLeader>yy :call pluto#yank_cell()<CR>
nnoremap <silent> <LocalLeader>dd :call pluto#delete_cell()<CR>

nnoremap <silent> <LocalLeader>P :call pluto#paste_cell_above()<CR>
nnoremap <silent> <LocalLeader>p :call pluto#paste_cell_below()<CR>

nnoremap <silent> <LocalLeader>t :call pluto#toggle_code()<CR>

nmap <F8> :TagbarToggle<CR>
"nmap <F12> :TransparentToggle<CR>
nmap :vertical resize 20:set winfixwidth

let g:livepreview_previewer = 'zathura'
"let g:livepreview_previewer = 'mupdf'
let g:livepreview_engine = 'pdflatex'
let g:livepreview_updatetime = '10'
:let hs_highlight_delimiters = 1

let g:lightline = {
      \ 'colorscheme': 'edge',
      \ }

filetype plugin indent on    " required

set expandtab
set shiftwidth=4
set softtabstop=4
set smartindent
set hlsearch

highlight Pmenu ctermfg=15 ctermbg=8 guifg=#0000ff guibg=#0000ff

" UltiSnips triggering
let g:UltiSnipsExpandTrigger = '<C-Space>'
let g:UltiSnipsJumpForwardTrigger = '<C-Space>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'

" Remappings
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"imap jk <esc>
imap kj <esc>

map ss zfa}
map ; :
map <A> w
map ` <F3>

lua require "lsp_signature".setup()

let g:mkdp_browser = '/usr/bin/surf'
let g:mkdp_echo_preview_url = 1



set showcmd

"highlight LineNr ctermfg=4 
highlight Comment ctermfg=8 cterm=italic
highlight Function ctermfg=4 cterm=italic

let g:tex_flavor = "latex"

" Math related abbreviations
au FileType tex iabbrev RR \mathbb{R}
au FileType tex iabbrev KK \mathbb{K}
au FileType tex iabbrev NN \mathbb{N}
au FileType tex iabbrev ZZ \mathbb{Z}
au FileType tex iabbrev QQ \mathbb{Q}
au FileType tex iabbrev FF \mathbb{F}
au FileType tex iabbrev inv ^{-1}
au FileType tex iabbrev iit \item
au FileType tex iabbrev ft \frametitle{
au FileType tex iabbrev pp \usepackage{
au FileType tex iabbrev sc \section{
au FileType tex iabbrev sss \subsection{
au FileType tex iabbrev gmtr \usepackage[a4paper, total={6in,10in}]{geometry}
au FileType tex iabbrev sc \section{
au FileType tex iabbrev ml \[<CR> <CR>\] <Esc>kddkA<CR><Tab>
au FileType tex iabbrev qq \Rightarrow
au FileType tex iabbrev ww \rightarrow

let g:vimtex_view_method = 'zathura'


"LaTeX

":nnoremap K i<CR><Esc>
nnoremap <F5> :UndotreeToggle<cr>
nmap <F8> :TagbarToggle<CR>

lua require'lspconfig'.clangd.setup{}
lua require'lspconfig'.hls.setup{}
lua require'lspconfig'.pylsp.setup{}
lua require'lspconfig'.julials.setup{}

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect

" Avoid showing message extra message when using completion
set shortmess+=c
nnoremap <silent> ca <cmd>lua vim.lsp.buf.code_action()<CR>
let g:completion_enable_auto_popup = 1

let g:indentLine_defaultGroup = 'SpecialKey'
nmap <F8> :TagbarToggle<CR>
nnoremap <F7> <cmd>TroubleToggle<cr>
"lua require('neoscroll').setup()

set mouse=a

"set conceallevel=2 

let g:tagbar_type_haskell = {
    \ 'ctagsbin'  : 'hasktags',
    \ 'ctagsargs' : '-x -c -o-',
    \ 'kinds'     : [
        \  'm:modules:0:1',
        \  'd:data: 0:1',
        \  'd_gadt: data gadt:0:1',
        \  't:type names:0:1',
        \  'nt:new types:0:1',
        \  'c:classes:0:1',
        \  'cons:constructors:1:1',
        \  'c_gadt:constructor gadt:1:1',
        \  'c_a:constructor accessors:1:1',
        \  'ft:function types:1:1',
        \  'fi:function implementations:0:1',
        \  'i:instance:0:1',
        \  'o:others:0:1'
    \ ],
    \ 'sro'        : '.',
    \ 'kind2scope' : {
        \ 'm' : 'module',
        \ 'c' : 'class',
        \ 'd' : 'data',
        \ 't' : 'type',
        \ 'i' : 'instance'
    \ },
    \ 'scope2kind' : {
        \ 'module'   : 'm',
        \ 'class'    : 'c',
        \ 'data'     : 'd',
        \ 'type'     : 't',
        \ 'instance' : 'i'
    \ }
\ }

lua << EOF

--lspconfig

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>gn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', vim.lsp.buf.formatting, bufopts)
end

require'nvim-treesitter.configs'.setup {
  ensure_installed = { "c", "lua", "rust" },
  sync_install = false,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
}

require("trouble").setup {
    height = 6,
    icons = false,
    fold_open = "v", -- icon used for open folds
    fold_closed = ">", -- icon used for closed folds
    indent_lines = false, -- add an indent guide below the fold icons
    signs = {
        -- icons / text used for a diagnostic
        error = "error",
        warning = "warn",
        hint = "hint",
        information = "info"
    },
    use_diagnostic_signs = false
  }

-- lsp cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local lspconfig = require('lspconfig')

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = { 'clangd', 'hls'}
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    -- on_attach = my_custom_on_attach,
    capabilities = capabilities,
  }
end

-- Setup nvim-cmp.
local cmp = require'cmp'

  cmp.setup{
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        --vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
        vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      end,
    },
    window = {
      -- completion = cmp.config.window.bordered(),
      -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-l>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      -- { name = 'vsnip' }, -- For vsnip users.
      -- { name = 'luasnip' }, -- For luasnip users.
      { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  }
cmp.setup {

  -- ... Your other configuration ...

  mapping = {

    -- ... Your other mappings ...

    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
         cmp.select_next_item()
    --  elseif vim.fn["vsnip#available"](1) == 1 then
    --    feedkey("<Plug>(vsnip-expand-or-jump)", "")
    --  elseif has_words_before() then
    --    cmp.complete()
      else
        fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
      end
    end, { "i", "s" }),

    ["<S-Tab>"] = cmp.mapping(function()
      if cmp.visible() then
        cmp.select_prev_item()
      elseif vim.fn["vsnip#jumpable"](-1) == 1 then
        feedkey("<Plug>(vsnip-jump-prev)", "")
      end
    end, { "i", "s" }),

    -- ... Your other mappings ...

  }

  -- ... Your other configuration ...

}

require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the four listed parsers should always be installed)
  ensure_installed = { "c", "haskell", "lua", "vim", "python"},

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  -- List of parsers to ignore installing (for "all")
  ignore_install = { "javascript" },

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    -- `false` will disable the whole extension
    -- enable = true,
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
     --disable = { "haskell", "rust", "vim" ,"latex"},
     disable = {"vim" ,"latex"},
    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}

require'nvim-treesitter.configs'.setup {
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn", -- set to `false` to disable one of the mappings
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  },
}

require('copilot').setup({
  panel = {
    enabled = true,
    auto_refresh = true,
    keymap = {
      jump_prev = "[[",
      jump_next = "]]",
      accept = "<CR>",
      refresh = "gr",
      open = "<M-CR>"
    },
    layout = {
      position = "bottom", -- | top | left | right
      ratio = 0.4
    },
  },
  suggestion = {
    enabled = true,
    auto_trigger = false,
    debounce = 75,
    keymap = {
      accept = "<M-l>",
      accept_word = false,
      accept_line = false,
      next = "<M-]>",
      prev = "<M-[>",
      dismiss = "<C-]>",
    },
  },
  filetypes = {
    yaml = false,
    markdown = false,
    help = false,
    gitcommit = false,
    gitrebase = false,
    hgcommit = false,
    svn = false,
    cvs = false,
    ["."] = false,
  },
  copilot_node_command = 'node', -- Node.js version must be > 18.x
  server_opts_overrides = {},
})

EOF

colorscheme edge
let maplocalleader = "\\"

let g:wheel#map#up   = '<c-k>'
let g:wheel#map#down = '<c-j>'
