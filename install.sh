# /bin/sh

mkdir -p $HOME/.config/i3/
mkdir -p $HOME/.config/nvim/
mkdir -p $HOME/.config/ranger/
mkdir -p $HOME/.config/fish/

cp i3config $HOME/.config/i3/config
cp config.fish $HOME/.config/fish/
cp scope.sh rifle.conf rc.conf commands.py $HOME/.config/ranger/
cp init.vim $HOME/.config/nvim/
